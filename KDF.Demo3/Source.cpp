#include <string>
#include <iostream>
#include <algorithm>
#include <conio.h>

using namespace std;

enum Rank
{
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Spades,
	Clubs,
	Hearts,
	Diamonds
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main(){

	Card c1;
	c1.rank = Two;
	c1.suit = Spades;

	Card c2;
	c2.rank = Three;
	c2.suit = Spades;

	Card c3;
	c3.rank = Four;
	c3.suit = Spades;

	Card c4;
	c4.rank = Five;
	c4.suit = Spades;

	Card c5;
	c5.rank = Six;
	c5.suit = Spades;

	Card c6;
	c6.rank = Seven;
	c6.suit = Spades;

	Card c7;
	c7.rank = Eight;
	c7.suit = Spades;

	Card c8;
	c8.rank = Nine;
	c8.suit = Spades;

	Card c9;
	c9.rank = Ten;
	c9.suit = Spades;

	Card c10;
	c10.rank = Jack;
	c10.suit = Spades;

	Card c11;
	c11.rank = Queen;
	c11.suit = Spades;

	Card c12;
	c12.rank = King;
	c12.suit = Spades;

	Card c13;
	c13.rank = Ace;
	c13.suit = Spades;

	Card c14;
	c14.rank = Two;
	c14.suit = Clubs;

	Card c15;
	c15.rank = Three;
	c15.suit = Clubs;

	Card c16;
	c16.rank = Four;
	c16.suit = Clubs;

	Card c17;
	c17.rank = Five;
	c17.suit = Clubs;

	Card c18;
	c18.rank = Six;
	c18.suit = Clubs;

	Card c19;
	c19.rank = Seven;
	c19.suit = Clubs;

	Card c20;
	c20.rank = Eight;
	c20.suit = Clubs;

	Card c21;
	c21.rank = Nine;
	c21.suit = Clubs;

	Card c22;
	c22.rank = Ten;
	c22.suit = Clubs;

	Card c23;
	c23.rank = Jack;
	c23.suit = Clubs;

	Card c24;
	c24.rank = Queen;
	c24.suit = Clubs;

	Card c25;
	c25.rank = King;
	c25.suit = Clubs;

	Card c26;
	c26.rank = Ace;
	c26.suit = Clubs;

	Card c27;
	c27.rank = Two;
	c27.suit = Hearts;

	Card c28;
	c28.rank = Three;
	c28.suit = Hearts;

	Card c29;
	c29.rank = Four;
	c29.suit = Hearts;

	Card c30;
	c30.rank = Five;
	c30.suit = Hearts;

	Card c31;
	c31.rank = Six;
	c31.suit = Hearts;

	Card c32;
	c32.rank = Seven;
	c32.suit = Hearts;

	Card c33;
	c33.rank = Eight;
	c33.suit = Hearts;

	Card c34;
	c34.rank = Nine;
	c34.suit = Hearts;

	Card c35;
	c35.rank = Ten;
	c35.suit = Hearts;

	Card c36;
	c36.rank = Jack;
	c36.suit = Hearts;

	Card c37;
	c37.rank = Queen;
	c37.suit = Hearts;

	Card c38;
	c38.rank = King;
	c38.suit = Hearts;

	Card c39;
	c39.rank = Ace;
	c39.suit = Hearts;

	Card c40;
	c40.rank = Two;
	c40.suit = Diamonds;

	Card c41;
	c41.rank = Three;
	c41.suit = Diamonds;

	Card c42;
	c42.rank = Four;
	c42.suit = Diamonds;

	Card c43;
	c43.rank = Five;
	c43.suit = Diamonds;

	Card c44;
	c44.rank = Six;
	c44.suit = Diamonds;

	Card c45;
	c45.rank = Seven;
	c45.suit = Diamonds;

	Card c46;
	c46.rank = Eight;
	c46.suit = Diamonds;

	Card c47;
	c47.rank = Nine;
	c47.suit = Diamonds;

	Card c48;
	c48.rank = Ten;
	c48.suit = Diamonds;

	Card c49;
	c49.rank = Jack;
	c49.suit = Diamonds;

	Card c50;
	c50.rank = Queen;
	c50.suit = Diamonds;

	Card c51;
	c51.rank = King;
	c51.suit = Diamonds;

	Card c52;
	c52.rank = Ace;
	c52.suit = Diamonds;
	
	_getch();
	return 0;
}